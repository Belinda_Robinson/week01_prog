﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise02
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Bee";
            var greeting = $"Hello {myName}, Have a great day!";

            Console.WriteLine(greeting);
        }
    }
}
