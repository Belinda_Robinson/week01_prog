﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication11
{
    class Program
    {
        static void Main(string[] args)
        {
            var length = int.Parse(Console.ReadLine());
            var breadth = int.Parse(Console.ReadLine());

            Console.WriteLine($"the area of a rectangle = {length * breadth}");

            Console.WriteLine($"the perimeter of a rectangle is = {(2 * length) + (2 * breadth)}");

            var r = int.Parse(Console.ReadLine());

            Console.WriteLine($"the area of a circle is = {3.14 * r * r}");

            Console.WriteLine($"the circumference of a value is = {2 * 3.14 * r}");
                }
    }
}
