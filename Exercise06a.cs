﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pick a number convert from KM to Miles");
            const double km2miles = 0.621371;
            var input = double.Parse(Console.ReadLine());

             var answer = Math.Round(input * km2miles, 4);

            Console.WriteLine($"{input} km is {answer} miles");
                        
        }
    }
}
