﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise03
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Bee";
            var greeting = $"Hello {myName}, Whats your Name?";

            Console.WriteLine(greeting);
            Console.ReadLine();
            Console.WriteLine("nice to meet you!");
        }
    }
}
